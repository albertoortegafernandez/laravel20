<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Modulo nº {{$module->id}}</h1>

    <ul>
        <li>
        <strong>Curso</strong>
            {{$module->course}}
        </li>
        <li>
        <strong>Nombre</strong>
            {{$module->name}}
        </li>
        <li>
        <strong>Codigo</strong>
            {{$module->code}}
        </li>
        <li>
        <strong>Nombre Corto</strong>
            {{$module->short_name}}
        </li>
        <li>
        <strong>Abreviatura</strong>
            {{$module->abreviation}}
        </li>
    </ul>
</body>
</html>