<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
<<<<<<< HEAD
use App\Models\Module;

class ModuleSeeder extends Seeder
{
    public function run()
    {
        Module::create([
            'course'=>'DAW2',
            'name'=>'Desarrollo web en entorno cliente',
            'code' => '0612',
            'short_name' => 'Cliente',
            'abreviation' => 'DWEC'
            ]);
        Module::create([
            'course'=>'DAW2',
            'name'=>'Desarrollo web en entorno servidor',
            'code' => '0613',
            'short_name' => 'Servidor',
            'abreviation' => 'DWES'
            ]);        
        Module::create([
            'course'=>'DAW2',
            'name'=>'Despliegue de aplicaciones web',
            'code' => '0614',
            'short_name' => 'Despliegue',
            'abreviation' => 'DEAW'
            ]);
        Module::create([
            'course'=>'DAW2',
            'name'=>'Disenio de interfaces web',
            'code' => '0615',
            'short_name' => 'Disenio',
            'abreviation' => 'DIW'
            ]);
        Module::create([
            'course'=>'DAW2',
            'name'=>'Lengua extranjera profesional: nivel2',
            'code' => 'A053',
            'short_name' => 'Ingles2',
            'abreviation' => 'ING2'
            ]);
        Module::create([
            'course'=>'DAW2',
            'name'=>'Empresa e iniciativa emprendedora',
            'code' => '0618',
            'short_name' => 'Empresa',
            'abreviation' => 'EIE'
            ]);
        Module::create([
            'course'=>'DAW2',
            'name'=>'Proyecto de desarrollo de aplicaciones web',
            'code' => '0616',
            'short_name' => 'Proyecto',
            'abreviation' => 'PDAW'
            ]);
        Module::create([
            'course'=>'DAW2',
            'name'=>'Formacion en centros de trabajo',
            'code' => '0619',
            'short_name' => 'Practicas',
            'abreviation' => 'FCT'
            ]);
        Module::create([
            'course'=>'DAM2',
            'name'=>'Acceso a datos',
            'code' => '0486',
            'short_name' => 'Datos',
            'abreviation' => 'ADA'
            ]);
        Module::create([
            'course'=>'DAM2',
            'name'=>'Programacion multimedia y dispositivos moviles',
            'code' => '0489',
            'short_name' => 'Multimedia y moviles',
            'abreviation' => 'PMDM'
            ]);        
        Module::create([
            'course'=>'DAM2',
            'name'=>'Programacion de servicios y procesos',
            'code' => '0490',
            'short_name' => 'Servicios y procesos',
            'abreviation' => 'PSP'
            ]);
        Module::create([
            'course'=>'DAM2',
            'name'=>'Desarrollo de interfaces',
            'code' => '0488',
            'short_name' => 'Interfaces',
            'abreviation' => 'DEI'
             ]);
        Module::create([
            'course'=>'DAM2',
            'name'=>'Lengua extranjera profesional: nivel2',
            'code' => 'A051',
            'short_name' => 'Ingles2',
            'abreviation' => 'ING2'
            ]);
        Module::create([
            'course'=>'DAM2',
            'name'=>'Empresa e iniciativa emprendedora',
            'code' => '0494',
            'short_name' => 'Empresa',
            'abreviation' => 'EIE'
            ]);
        Module::create([
            'course'=>'DAM2',
            'name'=>'Proyecto de desarrollo de aplicaciones multiplataforma',
            'code' => '0492',
            'short_name' => 'Proyecto',
            'abreviation' => 'PDAM'
            ]);
        Module::create([
            'course'=>'DAM2',
            'name'=>'Formacion en centros de trabajo',
            'code' => '0495',
            'short_name' => 'Practicas',
            'abreviation' => 'FCT'
            ]);
=======

class ModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
>>>>>>> clase10
    }
}
