<?php

namespace App\Http\Controllers;

use App\Models\Module;
<<<<<<< HEAD
=======
use App\Models\Study;
>>>>>>> clase10
use Illuminate\Http\Request;

class ModuleController extends Controller
{
<<<<<<< HEAD
=======
    public function __construct()
    {
        $this->middleware('auth');
    }
>>>>>>> clase10
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
<<<<<<< HEAD
        $modules= Module::all();
        return view('module.index',['modules'=>$modules]);
=======
        $modules = Module::all();
        return view('module.index', ['modules' => $modules]);
>>>>>>> clase10
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
<<<<<<< HEAD
        //
=======
        $studies = Study::all(); 
        return view('module.create', ['studies' => $studies]);
>>>>>>> clase10
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
<<<<<<< HEAD
        //
=======
        //validar
        $rules = [
            'code' => 'required|max:4',
            'name' => 'required',
            'hours' => 'integer',
            'study_id' => 'required|exists:studies,id',
        ];
        $request->validate($rules);
        //crear
        $module = Module::create($request->all());
        //redirigir
        return redirect('/modules');
    }

    public function storeToStudy($id, Request $request)
    {
        $rules = [
            'code' => 'required|max:4',
            'name' => 'required',
            'hours' => 'integer',
            // 'study_id' => 'required|exists:studies,id',
        ];
        $request->validate($rules);
        $module = new Module;
        $module->code = $request->code;
        $module->name = $request->name;
        $module->hours = $request->hours;
        $module->study_id = $id;
        $module->save();
        return back();
        // return redirect('/studies/' . $id);
>>>>>>> clase10
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Module  $module
     * @return \Illuminate\Http\Response
     */
    public function show(Module $module)
    {
<<<<<<< HEAD
        return view('module.show',['module'=>$module]);
=======
        //
>>>>>>> clase10
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Module  $module
     * @return \Illuminate\Http\Response
     */
    public function edit(Module $module)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Module  $module
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Module $module)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Module  $module
     * @return \Illuminate\Http\Response
     */
    public function destroy(Module $module)
    {
<<<<<<< HEAD
        //
    }
=======
        $module->delete();
        return back()->with('status', 'Modulo borrado');
    }
    // public function destroy($id)
    // {
    //     Module::destroy($id);
    //     return back()->with('status', 'Modulo borrado');
    // }
>>>>>>> clase10
}
